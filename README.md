# Regas Cypress setup

Example setup for Cypress including Typescript and Cucumber, used for E2E-testing at Regas

## Cypress
```
$ npm init
$ npm install cypress
```

Optional: add script to package.json
```json
 "scripts": {
    "cypress:run": "cypress run",
    "cypress:open": "cypress open"
  },
```

Test Cypress
```
$ npm run cypress:open
```
## Cucumber and Typescript
```
$ npm install cypress-cucumber-preprocessor typescript tsify @types/jest @types/node 
```
Rename *.js files to *.ts (except plugins/index.js)

In plugins/index.js load cucumber and typescript compilers so it compiles test files before running them

```javascript
/// <reference types="cypress" />
const cucumber = require('cypress-cucumber-preprocessor').default;
const browserify = require('@cypress/browserify-preprocessor');

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (on, config) => {
  const options = browserify.defaultOptions;
  options.browserifyOptions.plugin.unshift(['tsify']);

  on('file:preprocessor', browserify(options));
  on('file:preprocessor', cucumber(options));
  return config
}
```
In order to get typescript to compile we do need a tsconfig.json. Below the settings we use
```json
{
  "compilerOptions": {
    "baseUrl": "./node_modules",
    "target": "esnext",
    "lib": ["es5", "dom"],
    "types": ["cypress", "node"],
    "esModuleInterop": true,
    "module": "CommonJS",
    "typeRoots": ["node_modules/@types", "cypress"],
    "strictNullChecks": true,
    "noUnusedLocals": true,
    "noUnusedParameters": true
  },
  "include": ["./node_modules/cypress", "**/*.ts"]
}
```
What you can now do is in the cypress/integration folder define the things that have to be tested using Gherkin script. These scripts are easier to understand for non-developers. These steps then have to be implemented in a separate .spec.ts file in order for the test to work.
```gherkin
Feature: Example

  Scenario: Open regas blog
    Given the regas homepage is loaded
    When clicking the link "Lees meer op onze blog"
    Then the blog should be opened
```
At regas we choose to do this in a separate folder, to not confuse the non-developers reading the Gherkin-scripts. In order to do that define the following in package.json
```json
 "cypress-cucumber-preprocessor": {
    "step_definitions": "cypress/support/steps"
  },
```
Now the step-definitions can be included in files in the cypress/support/steps folder

```typescript
import { Given, Then, When } from 'cypress-cucumber-preprocessor/steps';

Given('the regas homepage is loaded', () => cy.visit('https://regas.nl'))

When('clicking the link {string}', (text: string) => cy.contains('a', text).scrollIntoView().click())

Then('the blog should be opened', () => cy.contains('Nooit te oud om te leren').should('be.visible'))
```
In order to make sure Cypress starts using these .feature files instead of looking for .spec files only we need to change its config in cypress.json
```json
{
  "testFiles": "**/*.{feature,spec.ts}"
}
```
Now we are ready to test!
```bash
npm run cypress:open
```
This setup with separate folders for Gherkin scripts and step-definitions also enables us to load the Gherkin script from another repository, which our QA officer to maintain for instance.
