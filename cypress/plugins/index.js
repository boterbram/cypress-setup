/// <reference types="cypress" />
const cucumber = require('cypress-cucumber-preprocessor').default;
const browserify = require('@cypress/browserify-preprocessor');

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (on, config) => {
  const options = { 
    ...browserify.defaultOptions,
    typescript: require.resolve('typescript')
  };
  options.browserifyOptions.plugin.unshift(['tsify']);

  on('file:preprocessor', browserify(options));
  on('file:preprocessor', cucumber(options));
  return config
}
