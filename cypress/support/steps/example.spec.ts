import { Given, Then, When } from 'cypress-cucumber-preprocessor/steps';

// Sadly regas.nl throws an exception when trying to use Electron to visit it at the moment
Cypress.on('uncaught:exception', (_, __) => {
  return false;
});

Given('the regas homepage is loaded', () => cy.visit('https://regas.nl'))

When('clicking the link {string}', (text: string) => cy.contains('a', text).scrollIntoView().click())

Then('the blog should be opened', () => cy.contains('Nooit te oud om te leren').should('be.visible'))
